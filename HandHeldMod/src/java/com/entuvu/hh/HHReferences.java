package com.entuvu.hh;

public class HHReferences {
	public static final String MOD_ID = "hh";
	public static final String MOD_NAME = "Hand Held";
	public static final String VERSION = "0.0.1";
	public static final String CLIENT_PROXY_CLASS = "com.entuvu.hh.proxy.ClientProxy";
	public static final String COMMON_PROXY_CLASS = "com.entuvu.hh.proxy.CommonProxy";
	public static final String SERVER_PROXY_CLASS = "com.entuvu.hh.proxy.ServerProxy";
	
	public enum GUIs{
		CHARGING_STATION
	}
	
}
