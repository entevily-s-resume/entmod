package com.entuvu.hh.proxy;

import com.entuvu.hh.render.blocks.HHRegisterRenderBlocks;
import com.entuvu.hh.render.items.HHRegisterRenderItems;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy {

    @Override
    public void preInit(FMLPreInitializationEvent e) {
        super.preInit(e);
    }

    @Override
    public void init(FMLInitializationEvent e) {
        super.init(e);
        
        HHRegisterRenderItems.registerItemRenderer();
        HHRegisterRenderBlocks.registerBlockRenderer();
    }

    @Override
    public void postInit(FMLPostInitializationEvent e) {
        super.postInit(e);
    }
		
}
