package com.entuvu.hh.proxy;

import com.entuvu.hh.blocks.HHBlocks;
import com.entuvu.hh.config.HHConfig;
import com.entuvu.hh.crafting.HHCrafting;
import com.entuvu.hh.gui.HHGuiHandler;
import com.entuvu.hh.items.HHItems;
import com.entuvu.hh.tileentities.HHTileEntities;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class CommonProxy {
	public void preInit(FMLPreInitializationEvent e) {
		HHItems.registerItems();
		HHBlocks.registerBlocks();
		HHTileEntities.registerTileEntities();
		HHConfig.getConfig(e);
    }

    public void init(FMLInitializationEvent e) {
    	HHGuiHandler.registerGUI();
    	HHCrafting.initCrafting();
    }

    public void postInit(FMLPostInitializationEvent e) {

    }
}

