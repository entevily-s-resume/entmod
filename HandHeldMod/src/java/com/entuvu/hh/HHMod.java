package com.entuvu.hh;

import com.entuvu.hh.proxy.CommonProxy;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = HHReferences.MOD_ID, name = HHReferences.MOD_NAME, version = HHReferences.VERSION)
public class HHMod {
	
	@Instance(value=HHReferences.MOD_ID)
	public static HHMod instance = new HHMod();
	
	@SidedProxy(clientSide=HHReferences.CLIENT_PROXY_CLASS, serverSide=HHReferences.SERVER_PROXY_CLASS)
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit (FMLPreInitializationEvent event){
		proxy.preInit(event);
	}

	@EventHandler
	public void init (FMLInitializationEvent event){
		proxy.init(event);
	}
	
	@EventHandler
	public void postInit (FMLPostInitializationEvent event){
		proxy.postInit(event);
	}
}
