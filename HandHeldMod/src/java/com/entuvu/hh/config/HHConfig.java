package com.entuvu.hh.config;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class HHConfig {
	
	public static int randomBlockID;
    public static int randomItemID;

    public static Block randomBlock;
    public static Item  randomItem;
    public static boolean someConfigFlag;
    
	public static void getConfig(FMLPreInitializationEvent event){
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		
		config.load();
		
		
		
		config.save();
	}
}
