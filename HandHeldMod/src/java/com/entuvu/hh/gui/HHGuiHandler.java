package com.entuvu.hh.gui;

import com.entuvu.hh.HHMod;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.common.network.NetworkRegistry;

public class HHGuiHandler implements IGuiHandler {

	public static void registerGUI(){
		NetworkRegistry.INSTANCE.registerGuiHandler(HHMod.instance, new HHGuiHandler());
	}
	
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		return null;
	}

}
