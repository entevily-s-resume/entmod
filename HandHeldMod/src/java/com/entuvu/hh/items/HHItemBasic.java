package com.entuvu.hh.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class HHItemBasic extends Item {
	public HHItemBasic(String unlocalizedName){
		super();
		
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(CreativeTabs.tabMaterials);
	}
}
