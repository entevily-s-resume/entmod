package com.entuvu.hh.items;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public final class HHItems {
	public static Item metal_detector;
	
	public static void registerItems(){
		GameRegistry.registerItem(metal_detector = new HHItemMetalDetector("metal_detector"), "metal_detector");
	}
	
}
