package com.entuvu.hh.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class HHContainerChargingStation extends Container {

	private final IInventory tileChargingStation;
	private int field_178152_f;
	private int field_178154_h;
	private int field_178155_i;
	private int field_178153_g;
    
	public HHContainerChargingStation(InventoryPlayer playerInventory, IInventory chargingStationInventory) {
		this.tileChargingStation = chargingStationInventory;
        this.addSlotToContainer(new Slot(chargingStationInventory, 0, 56, 17));
        this.addSlotToContainer(new Slot(chargingStationInventory, 1, 116, 35));
        int i;

        for (i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInventory, i, 8 + i * 18, 142));
        }
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return this.tileChargingStation.isUseableByPlayer(playerIn);
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting listener) {
		super.addCraftingToCrafters(listener);
        listener.func_175173_a(this, this.tileChargingStation);
	}
	
	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);

            if (this.field_178152_f != this.tileChargingStation.getField(2))
            {
                icrafting.sendProgressBarUpdate(this, 2, this.tileChargingStation.getField(2));
            }

            if (this.field_178154_h != this.tileChargingStation.getField(0))
            {
                icrafting.sendProgressBarUpdate(this, 0, this.tileChargingStation.getField(0));
            }

            if (this.field_178155_i != this.tileChargingStation.getField(1))
            {
                icrafting.sendProgressBarUpdate(this, 1, this.tileChargingStation.getField(1));
            }

            if (this.field_178153_g != this.tileChargingStation.getField(3))
            {
                icrafting.sendProgressBarUpdate(this, 3, this.tileChargingStation.getField(3));
            }
        }

        this.field_178152_f = this.tileChargingStation.getField(2);
        this.field_178154_h = this.tileChargingStation.getField(0);
        this.field_178155_i = this.tileChargingStation.getField(1);
        this.field_178153_g = this.tileChargingStation.getField(3);
	}

	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int id, int data) {
		this.tileChargingStation.setField(id, data);
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		// TODO Auto-generated method stub
		return super.transferStackInSlot(playerIn, index);
	}

}
