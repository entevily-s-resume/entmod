package com.entuvu.hh.tileentities;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFurnace;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerFurnace;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.server.gui.IUpdatePlayerListBox;
import net.minecraft.tileentity.TileEntityLockable;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;

public class HHChargingStationTE extends TileEntityLockable implements ISidedInventory, IUpdatePlayerListBox {
	
	private static final int[] slotsTop = new int[] {0};
    private static final int[] slotsBottom = new int[] {1};
    private static final int[] slotsSides = new int[] {1};
    /** The ItemStacks that hold the items currently being used in the charging station */
	private ItemStack[] chargingStationItemStacks = new ItemStack[2];
    /** The number of ticks that the furnace will keep burning */
    private int chargingStationChargeTime;
    /** The number of ticks that a fresh copy of the currently-burning item would keep the furnace burning for */
    private int currentItemChargeTime;
    private int chargeTime;
    private int totalChargeTime;
    private String chargingStationCustomName;

    
    // Returns the number of slots in the inventory.
    @Override
    public int getSizeInventory() {
    	return this.chargingStationItemStacks.length;
    }
    
    // Returns the stack in slot i
    @Override
    public ItemStack getStackInSlot(int index) {
    	return this.chargingStationItemStacks[index];
    }
    
    // Removes from an inventory slot up to the spec number of items and in new stack. first minus second arg.
    @Override
    public ItemStack decrStackSize(int index, int count) {
    	if(this.chargingStationItemStacks[index] != null){
    		ItemStack itemstack;
    		if(this.chargingStationItemStacks[index].stackSize <=count ){
    			itemstack = this.chargingStationItemStacks[index];
    			this.chargingStationItemStacks[index] = null;
    			return itemstack;
    		}
    		else
    		{
    			itemstack = this.chargingStationItemStacks[index].splitStack(count);
    			
    			if(this.chargingStationItemStacks[index].stackSize == 0){
    				this.chargingStationItemStacks[index] = null;
    			}
    			return itemstack;
    		}
    	}
    	else
    	{
    		return null;
    	}
    }

	@Override
	public ItemStack getStackInSlotOnClosing(int index) {
		if (this.chargingStationItemStacks[index] != null)
        {
            ItemStack itemstack = this.chargingStationItemStacks[index];
            this.chargingStationItemStacks[index] = null;
            return itemstack;
        }
        else
        {
            return null;
        }
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		boolean flag = stack != null && stack.isItemEqual(this.chargingStationItemStacks[index]) && ItemStack.areItemStackTagsEqual(stack, this.chargingStationItemStacks[index]);
        this.chargingStationItemStacks[index] = stack;

        if (stack != null && stack.stackSize > this.getInventoryStackLimit())
        {
            stack.stackSize = this.getInventoryStackLimit();
        }

        if (index == 0 && !flag)
        {
            this.totalChargeTime = this.func_174904_a(stack);
            this.chargeTime = 0;
            this.markDirty();
        }
	}
	
	public int func_174904_a(ItemStack p_174904_1_)
    {
        return 200;
    }

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		 return this.worldObj.getTileEntity(this.pos) != this ? false : player.getDistanceSq((double)this.pos.getX() + 0.5D, (double)this.pos.getY() + 0.5D, (double)this.pos.getZ() + 0.5D) <= 64.0D;
	}

	@Override
	public void openInventory(EntityPlayer player) {}

	@Override
    public void closeInventory(EntityPlayer player) {}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getField(int id) {
		switch (id)
        {
            case 0:
                return this.chargingStationChargeTime;
            case 1:
                return this.currentItemChargeTime;
            case 2:
                return this.chargeTime;
            case 3:
                return this.totalChargeTime;
            default:
                return 0;
        }
	}

	@Override
	public void setField(int id, int value) {
		switch (id)
        {
            case 0:
                this.chargingStationChargeTime = value;
                break;
            case 1:
                this.currentItemChargeTime = value;
                break;
            case 2:
                this.chargeTime = value;
                break;
            case 3:
                this.totalChargeTime = value;
        }
	}

	@Override
	public int getFieldCount() {
		 return 4;
	}

	@Override
	public void clear() {
		for (int i = 0; i < this.chargingStationItemStacks.length; ++i)
        {
            this.chargingStationItemStacks[i] = null;
        }
	}

	@Override
	public String getName() {
		return this.hasCustomName() ? this.chargingStationCustomName : "container.charging_station";
	}

	@Override
	public boolean hasCustomName() {
		return this.chargingStationCustomName != null && this.chargingStationCustomName.length() > 0;
	}

	@Override
	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn) {
		// This is part we need to update - look at Container Furnace. 
		return new ContainerFurnace(playerInventory, this);
	}

	@Override
	public String getGuiID() {
		return "hh:charging_station";
	}
	
	public boolean isCharging()
    {
        return this.chargingStationChargeTime > 0;
    }

	@Override
	public void update() {
		boolean flag = this.isCharging();
        boolean flag1 = false;

        if (this.isCharging())
        {
            --this.chargingStationChargeTime;
        }

        if (!this.worldObj.isRemote)
        {
            if (!this.isCharging() && (this.chargingStationItemStacks[1] == null || this.chargingStationItemStacks[0] == null))
            {
                if (!this.isCharging() && this.chargeTime > 0)
                {
                    this.chargeTime = MathHelper.clamp_int(this.chargeTime - 2, 0, this.totalChargeTime);
                }
            }
            else
            {
                if (!this.isCharging() && this.canCharge())
                {
                    this.currentItemChargeTime = this.chargingStationChargeTime = getItemBurnTime(this.chargingStationItemStacks[1]);

                    if (this.isCharging())
                    {
                        flag1 = true;

                        if (this.chargingStationItemStacks[1] != null)
                        {
                            --this.chargingStationItemStacks[1].stackSize;

                            if (this.chargingStationItemStacks[1].stackSize == 0)
                            {
                                this.chargingStationItemStacks[1] = chargingStationItemStacks[1].getItem().getContainerItem(chargingStationItemStacks[1]);
                            }
                        }
                    }
                }

                if (this.isCharging() && this.canCharge())
                {
                    ++this.chargeTime;

                    if (this.chargeTime == this.totalChargeTime)
                    {
                        this.chargeTime = 0;
                        this.totalChargeTime = this.func_174904_a(this.chargingStationItemStacks[0]);
                        this.chargeItem();
                        flag1 = true;
                    }
                }
                else
                {
                    this.chargeTime = 0;
                }
            }

            if (flag != this.isCharging())
            {
                flag1 = true;
                BlockFurnace.setState(this.isCharging(), this.worldObj, this.pos);
            }
        }

        if (flag1)
        {
            this.markDirty();
        }
	}
	
	

	private void chargeItem() {
		if (this.canCharge())
        {
            ItemStack itemstack = FurnaceRecipes.instance().getSmeltingResult(this.chargingStationItemStacks[0]);

            if (this.chargingStationItemStacks[2] == null)
            {
                this.chargingStationItemStacks[2] = itemstack.copy();
            }
            else if (this.chargingStationItemStacks[2].getItem() == itemstack.getItem())
            {
                this.chargingStationItemStacks[2].stackSize += itemstack.stackSize; // Forge BugFix: Results may have multiple items
            }

            if (this.chargingStationItemStacks[0].getItem() == Item.getItemFromBlock(Blocks.sponge) && this.chargingStationItemStacks[0].getMetadata() == 1 && this.chargingStationItemStacks[1] != null && this.chargingStationItemStacks[1].getItem() == Items.bucket)
            {
                this.chargingStationItemStacks[1] = new ItemStack(Items.water_bucket);
            }

            --this.chargingStationItemStacks[0].stackSize;

            if (this.chargingStationItemStacks[0].stackSize <= 0)
            {
                this.chargingStationItemStacks[0] = null;
            }
        }
	}

	private boolean canCharge() {
		return true;
	}

	private int getItemBurnTime(ItemStack itemStack) {
		if (itemStack == null)
        {
            return 0;
        }
        else
        {
            Item item = itemStack.getItem();

            if (item instanceof ItemBlock && Block.getBlockFromItem(item) != Blocks.air)
            {
                Block block = Block.getBlockFromItem(item);

                if (block == Blocks.wooden_slab)
                {
                    return 150;
                }

                if (block.getMaterial() == Material.wood)
                {
                    return 300;
                }

                if (block == Blocks.coal_block)
                {
                    return 16000;
                }
            }

            if (item instanceof ItemTool && ((ItemTool)item).getToolMaterialName().equals("WOOD")) return 200;
            if (item instanceof ItemSword && ((ItemSword)item).getToolMaterialName().equals("WOOD")) return 200;
            if (item instanceof ItemHoe && ((ItemHoe)item).getMaterialName().equals("WOOD")) return 200;
            if (item == Items.stick) return 100;
            if (item == Items.coal) return 1600;
            if (item == Items.lava_bucket) return 20000;
            if (item == Item.getItemFromBlock(Blocks.sapling)) return 100;
            if (item == Items.blaze_rod) return 2400;
            return net.minecraftforge.fml.common.registry.GameRegistry.getFuelValue(itemStack);
        }
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		return side == EnumFacing.DOWN ? slotsBottom : (side == EnumFacing.UP ? slotsTop : slotsSides);
	}

	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction) {
		 return this.isItemValidForSlot(index, itemStackIn);
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		if (direction == EnumFacing.DOWN && index == 1)
        {
            Item item = stack.getItem();

            if (item != Items.water_bucket && item != Items.bucket)
            {
                return false;
            }
        }

        return true;
	}
}
